﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using csharp4_lesson12.Models;
using csharp4_lesson12.Repositories;

namespace csharp4_lesson12.Models
{
    public class ApplicantInitializer : DropCreateDatabaseIfModelChanges<ApplicantContext>
    {
        protected override void Seed(ApplicantContext context)
        {
            var applicants = new List<Applicant>()
            {
                new Applicant{FirstName = "John", LastName="Doe", MiddleName="A", age=30},
                new Applicant{FirstName = "Jane", LastName="Doe", MiddleName="A", age=30},
            };
            applicants.ForEach(x=>context.applicants.Add(x));
            context.SaveChanges();
        }

    }
}
