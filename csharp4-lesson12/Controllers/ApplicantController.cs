﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson12.Repositories;
using csharp4_lesson12.Models;
using csharp4_lesson12.Interfaces;

namespace csharp4_lesson12.Controllers
{
    public class ApplicantController : Controller
    {
        private IApplicantsRepository ar;
        public ApplicantController()
        {
            ar = new ApplicantsRepository(new ApplicantContext());
        }

        public ApplicantController(IApplicantsRepository apprepo)
        {
            ar = apprepo;
        }
        public ActionResult Index()
        {
            var app = ar.GetAllApplicants();
            return View(app);
        }

        public ActionResult Search(string query="")
        {
            var app = ar.Search(query);
            return View("Index", app);
        }

        public ActionResult SearchByFirstName(string fn="")
        {
            if (!String.IsNullOrEmpty(fn))
            {
                var app = ar.SearchApplicantsByFirstName(x => x.FirstName.ToUpper() == fn.ToUpper());
                return View("Index", app);
            }
            else
            {
                var apps = ar.GetAllApplicants();
                return View("Index", apps);
            }
        }

        public ActionResult SearchByLastName(string ln = "")
        {
            if (!String.IsNullOrEmpty(ln))
            {
                var app = ar.SearchApplicantsByFirstName(x => x.LastName.ToUpper() == ln.ToUpper());
                return View("Index", app);
            }
            else
            {
                var apps = ar.GetAllApplicants();
                return View("Index", apps);
            }
        }

        public ActionResult SearchByMiddleName(string mid = "")
        {
            if (!String.IsNullOrEmpty(mid))
            {
                var app = ar.SearchApplicantsByFirstName(x => x.MiddleName.ToUpper() == mid.ToUpper());
                return View("Index", app);
            }
            else
            {
                var apps = ar.GetAllApplicants();
                return View("Index", apps);
            }
        }

        public ActionResult SearchByProfession(string pro = "")
        {
            if (!String.IsNullOrEmpty(pro))
            {
                var app = ar.SearchApplicantsByFirstName(x => x.Profession.ToUpper() == pro.ToUpper());
                return View("Index", app);
            }
            else
            {
                var apps = ar.GetAllApplicants();
                return View("Index", apps);
            }
        }

        public ActionResult OrderByFirstName()
        {
            ViewBag.sort = "firstname";
            return View(ar.OrderByFirstName());
        }

        public ActionResult OrderByLastName()
        {
            ViewBag.sort = "lastname";
            return View(ar.OrderByLastName());
        }

        public ActionResult OrderByMiddleName()
        {
            ViewBag.sort = "middlename";
            return View(ar.OrderByMiddleName());
        }


        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(Applicant app)
        {
            if (ModelState.IsValid)
            {
                ar.Create(app);
                ar.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(app);
        }

        public ActionResult Edit(int id=0)
        {
            Applicant app = ar.GetByApplicantNumber(id);
                if (app==null)
                {
                    return HttpNotFound();
                }
            return View(app);
        }

        [HttpPost]
        public ActionResult Edit(Applicant ap)
        {
            if (ModelState.IsValid)
            {
                ar.Edit(ap);
                ar.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ap);
        }

        public ActionResult Delete(int id=0)
        {
            Applicant app = ar.GetByApplicantNumber(id);
            if (app == null)
            {
                return HttpNotFound();
            }
            return View(app);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ar.Delete(id);
            ar.SaveChanges();
            return RedirectToAction("Index");
        }        

        protected override void Dispose(bool disposing)
        {
            ar.Dispose();
            base.Dispose(disposing);
        }

    }
}
