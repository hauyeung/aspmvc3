﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using csharp4_lesson12.Models;

namespace csharp4_lesson12.Interfaces
{
    public interface IApplicantsRepository : IDisposable 
    {
        IEnumerable<Applicant> GetAllApplicants();
        IQueryable<Applicant> SearchApplicantsByProfession(Expression<Func<Applicant, bool>> query);
        IQueryable<Applicant> SearchApplicantsByFirstName(Expression<Func<Applicant, bool>> query);
        IQueryable<Applicant> SearchApplicantsByMiddleName(Expression<Func<Applicant, bool>> query);
        IQueryable<Applicant> SearchApplicantsByLastName(Expression<Func<Applicant, bool>> query);
        IQueryable<Applicant> SearchApplicantsByAgeRange(int val1, int val2);
        void Create(Applicant app);
        void Edit(Applicant app);
        void Delete(int id);
        Applicant GetByApplicantNumber(int id);
        void SaveChanges();
        IEnumerable<Applicant> OrderByFirstName();
        IEnumerable<Applicant> OrderByLastName();
        IEnumerable<Applicant> OrderByMiddleName();
        IQueryable<Applicant> Search(string q);
    }
}
