﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Linq.Expressions;
using csharp4_lesson12.Interfaces;
using csharp4_lesson12.Models;


namespace csharp4_lesson12.Repositories
{
    public class ApplicantsRepository : IApplicantsRepository, IDisposable
    {
        private ApplicantContext app;
        public ApplicantsRepository(ApplicantContext con)
        {
            app = con;
        }

        public IQueryable<Applicant> Search(string q)
        {
            return app.applicants.Where(obj => (obj.FirstName.ToUpper()==q.ToUpper() || obj.LastName.ToUpper() ==q.ToUpper() || obj.MiddleName.ToUpper() == q.ToUpper() || obj.Profession.ToUpper() == q.ToUpper()) || obj.age.ToString() == q);
        }

        public IEnumerable<Applicant> GetAllApplicants()
        {
            return app.applicants.ToList();
        }

        public IQueryable<Applicant> SearchApplicantsByFirstName(Expression<Func<Applicant, bool>> query)
        {
            return app.applicants.Where(query);
        }

        public IQueryable<Applicant> SearchApplicantsByLastName(Expression<Func<Applicant, bool>> query)
        {
            return app.applicants.Where(query);
        }

        public IQueryable<Applicant> SearchApplicantsByMiddleName(Expression<Func<Applicant, bool>> query)
        {
            return app.applicants.Where(query);
        }

        public IQueryable<Applicant> SearchApplicantsByProfession(Expression<Func<Applicant, bool>> query)
        {
            return app.applicants.Where(query);
        }

        public IQueryable<Applicant> SearchApplicantsByAgeRange(int val1, int val2)
        {
            return app.applicants.Where(obj => (obj.age > val1 && obj.age < val2)).AsQueryable();
        }

        public IEnumerable<Applicant> OrderByFirstName()
        {
            var list = GetAllApplicants();
            return list.OrderBy(x => x.FirstName).AsQueryable();
        }

        public IEnumerable<Applicant> OrderByLastName()
        {
            var list = GetAllApplicants();
            return list.OrderBy(x => x.LastName).AsQueryable();
        }

        public IEnumerable<Applicant> OrderByMiddleName()
        {
            var list = GetAllApplicants();
            return list.OrderBy(x => x.MiddleName).AsQueryable();
        }

        public void Create(Applicant applicant)
        {
            app.applicants.Add(applicant);
        }

        public void Edit(Applicant applicant)
        {
            app.Entry(applicant).State = EntityState.Modified;
        }

        public void Delete(int id = 0)
        {
            Applicant applicant = app.applicants.Find(id);
            app.applicants.Remove(applicant);
        }

        public Applicant GetByApplicantNumber(int id=0)
        {
            Applicant applicant = app.applicants.Find(id);
            return applicant;
        }

        public void SaveChanges()
        {
            app.SaveChanges();
        }

        protected virtual void Dispose(bool disposal)
        {

                if (disposal)
                {
                    app.Dispose();
                }
            
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }      



    }
}
