﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace csharp4_lesson12.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString SortActionLink(this HtmlHelper htmlhelper, string text, string action, string controller)
        {            
            var currentaction = (string) htmlhelper.ViewContext.RouteData.Values["action"];
            if (action.ToUpper()==currentaction.ToUpper())
            {              
                return htmlhelper.ActionLink(text, action, controller, null, new { @class = "highlight" });         
            }
            return htmlhelper.ActionLink(text, action, controller);    
        }
    }
}
